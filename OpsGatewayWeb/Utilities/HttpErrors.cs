﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OpsGatewayWeb.Utilities
{
    public static partial class Http
    {
        public static class Errors
        {
            public static class BadJsonBody
            {
                public static string Message = "The JSON Body passed in does not meet the requirements.";
                public static int ErrorStatus = (int)HttpStatusCode.BadRequest;
            }
        }
    }
}
