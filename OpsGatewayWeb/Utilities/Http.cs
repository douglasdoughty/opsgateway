﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OpsGatewayWeb.Models;

namespace OpsGatewayWeb.Utilities
{
    public static partial class Http
    {
        public static LoginResponse GenerateLoginResponse(HttpRequest request, UserInformation userInfo)
        {
            if (!userInfo.IsValidUserInformation())
                return InvalidPostBody();

            var x = new LoginResponse
            {
                AuthorizedPreSharedKey = AuthorizePreSharedKey(request),
                ViewName = GetViewName(userInfo)
            };
            //var x = GetViewName(request);
            return new LoginResponse();
            
        }

        private static LoginResponse InvalidPostBody()
        {
            return new LoginResponse
            {
                ErrorMessage = Errors.BadJsonBody.Message,
                HttpStatusCode = Errors.BadJsonBody.ErrorStatus
            };
        }

        private static string GetViewName(UserInformation userInfo)
        {
            return userInfo.Organization;
        }

        private static bool AuthorizePreSharedKey(HttpRequest request)
        {
            var preSharedKey = GetPreSharedKey(request);
            return IsPreSharedKeyAuthorized(preSharedKey);

        }

        private static bool IsPreSharedKeyAuthorized(string preSharedKey)
        {
            //Database call
            return true;
        }

        private static string GetPreSharedKey(HttpRequest request)
        {
            return request.Headers["Authorization"];
        }


    }
}
