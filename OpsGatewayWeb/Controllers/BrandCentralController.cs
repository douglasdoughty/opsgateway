using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OpsGatewayWeb.Models;

namespace OpsGatewayWeb.Controllers
{
    public class BrandCentralController : Controller
    {

        // POST: BrandCentral/Login
        [HttpPost]
        public ActionResult Login([FromBody]UserInformation userInfo)
        {

            var response = Utilities.Http.GenerateLoginResponse(Request, userInfo);
            Response.StatusCode = response.HttpStatusCode;
            return View(response.ViewName, response);
        }

        //// GET: BrandCentral/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        //// GET: BrandCentral/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: BrandCentral/Create
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create(IFormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: BrandCentral/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: BrandCentral/Edit/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit(int id, IFormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: BrandCentral/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: BrandCentral/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Delete(int id, IFormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}