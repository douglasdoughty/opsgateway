﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpsGatewayWeb.Models
{
    public class UserInformation
    {
        public string Organization { get; set; }
        public string UserId { get; set; }

        /// <summary>
        /// Determines if the passed in UserInformation data is valid to be processed further.
        /// </summary>
        /// <returns></returns>
        public bool IsValidUserInformation()
        {
            if (string.IsNullOrEmpty(Organization))
                return false;

            if (string.IsNullOrEmpty(UserId))
                return false;

            return true;
        }

    }
}
