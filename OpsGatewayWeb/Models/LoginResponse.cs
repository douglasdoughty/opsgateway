﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpsGatewayWeb.Models
{
    public class LoginResponse
    {
        public string ViewName { get; set; }
        public bool AuthorizedPreSharedKey { get; set; }
        public bool UserExists { get; set; }
        public int HttpStatusCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}